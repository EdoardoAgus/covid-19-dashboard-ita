      
fetch('https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json')
.then(response => response.json())
.then(dati => {
  
  //ordino dati da più recenti a meo recenti
  let sorted = dati.reverse();
  
  //data ultimo aggiornamento
  let lastUpdated = sorted[0].data
  //formattazione data ultimo update
  let lastUpdatedFormatted = lastUpdated.split("T")[0].split("-").reverse().join("/")
  document.querySelector("#data").innerHTML= lastUpdatedFormatted
  
  let lastUpdatedData= sorted.filter(el => el.data == lastUpdated).sort( (a,b) => b.nuovi_positivi - a.nuovi_positivi);
  
  let totalCases = lastUpdatedData.map(el=>el.totale_casi).reduce((t,n)=> t+n)
  document.querySelector("#totalCases").innerHTML = totalCases
  
  let totalRecovered = lastUpdatedData.map(el=>el.dimessi_guariti).reduce((t,n)=>t+n)
  document.querySelector("#totalRecovered").innerHTML = totalRecovered
  
  let totalDeath = lastUpdatedData.map(el=>el.deceduti).reduce((t,n)=>t+n)
  document.querySelector("#totalDeath").innerHTML = totalDeath
  
  let totalPositive = lastUpdatedData.map(el=>el.totale_positivi).reduce((t,n)=>t+n)
  document.querySelector("#totalPositive").innerHTML = totalPositive
  
  let totalHospitalized = lastUpdatedData.map(el=>el.totale_ospedalizzati).reduce((t,n)=>t+n)
  document.querySelector(".totalHospitalized").innerHTML = totalHospitalized

  let totalIntensiveCare = lastUpdatedData.map(el=>el.terapia_intensiva).reduce((t,n)=>t+n)
  document.querySelector(".totalIntensive").innerHTML = totalIntensiveCare

  // suddivisione in giorni
  let days = Array.from(new Set(sorted.map(el => el.data))) 

  totalsPerDays = days.map( el => [el, sorted.filter( i => i.data == el).map(e => e.deceduti).reduce((t,n) => t+n)])
 
  //deceduti oggi
  let decedutiOggi = totalsPerDays[0][1] - totalsPerDays[1][1]
  document.querySelector('#todayDeceeds').innerHTML = decedutiOggi


  let cardWrapper = document.querySelector('#cardWrapper')
  let progressWrapper = document.querySelector('#progressWrapper')
  
  let todayMax = Math.max(...lastUpdatedData.map(el=>el.nuovi_positivi))

    
  lastUpdatedData.forEach(el => {
    
    let div = document.createElement('div')
    div.classList.add('col-12', 'my-4')
    div.innerHTML = 
      `
      <div class="card-custom p-3 pb-0 h-100 data-region"${el.denominazione_regione}>
        <p>${el.denominazione_regione}</p>
        <p class="text-right h-5 text-main">
        <span class="text-second"> Positivi: </span> ${el.nuovi_positivi}
        </p>
        <p class="text-right h-5 text-main">
        <span class="text-second"> Decessi: </span> ${el.deceduti}
        </p>
        <p class="text-right h-5 text-main">
        <span class="text-second"> Terapia intensiva: </span> ${el.terapia_intensiva}
        </p>
          
      </div>
          
      `
        cardWrapper.appendChild(div)
        
        let bar = document.createElement('div')
        bar.classList.add('col-12','mb-5')
        bar.innerHTML = 
        `
        <p class="mb-0">${el.denominazione_regione} : ${el.nuovi_positivi}</p>
        <div class="progress rounded-0">
          <div class="progress-bar bg-main " style="width: ${(el.nuovi_positivi/todayMax) * 100}%">
            
            </div>
            </div>
            `
            progressWrapper.appendChild(bar)
            
            let modal = document.querySelector('.modal-custom')
            let modalContent = document.querySelector('.modal-custom-content')
            
            document.querySelectorAll('[data-region').forEach(el=>{
              el.addEventListener('click', ()=>{
                
                let region = el.dataset.region
                // console.log(region);
                modal.classList.add('active')
                
                
              })
            })  
          })
          
          


          })

          
